package com.Bulbisoft.RapidPeonGenerator_Back.Controller;

import com.Bulbisoft.RapidPeonGenerator_Back.Controller.Representation.PeonRepresentation;
import com.Bulbisoft.RapidPeonGenerator_Back.Controller.Representation.PeonRepresentationFullInfo;
import com.Bulbisoft.RapidPeonGenerator_Back.Controller.Representation.PeonRepresentationNameClass;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Peon;
import com.Bulbisoft.RapidPeonGenerator_Back.Service.PeonService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/peons")
public class PeonController {
    PeonService service;

    public PeonController(PeonService service) {
        this.service = service;
    }

    @DeleteMapping("/delete/{id}")
    public String deleteOnePeonById(@PathVariable(value = "id") String id) {
        Optional<Peon> deletedPeon = service.getOnePeonById(id);
        service.deletePeonById(id);
        return deletedPeon.map(peon -> "Votre personnage " + peon.getName() + " a bien été supprimé").orElse("Personnage introuvable");
    }

    @PostMapping("/create/manual")
    public PeonRepresentationFullInfo createNewPeonManual(@RequestBody PeonRepresentation body) {
        PeonRepresentationFullInfo newPeon = service.createManualPeon(body);
        return newPeon;
    }

    @PostMapping("/create/auto")
    public PeonRepresentationFullInfo createNewPeonAuto(@RequestBody PeonRepresentationNameClass body) throws IOException {
        PeonRepresentationFullInfo newPeon = service.createNewAutoPeon(body.getName(), body.getClassPeon());
        return newPeon;
    }

    @GetMapping
    public List<PeonRepresentationFullInfo> getAllPeon() {
        List<PeonRepresentationFullInfo> peonsFull = new ArrayList<>();
        for (Peon p : service.getAllPeons()) {
            PeonRepresentationFullInfo peonFull = new PeonRepresentationFullInfo(p.getId(), p.getName(), p.getSex(),
                    p.getAge(), p.getRace(), p.getClassPeon() ,p.getStrength(), p.getEndurance(), p.getDexterity(),
                    p.getIntelligence(), p.getLuck());
            peonsFull.add(peonFull);
        }
        return peonsFull;
    }

    @GetMapping("/{id}")
    Peon getOnePeonById(@PathVariable(value = "id") String id) {
        Optional<Peon> peon = this.service.getOnePeonById(id);
        return peon.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Fiche de personnage inexistante"));
    }

    @PutMapping("/{idpe}/jointoplayer/{idpl}")
    public String addPeonToPlayer(@PathVariable(value = "idpe") String idpe, @PathVariable(value = "idpl") String idpl) {
        service.AddPeonToPlayer(idpe, idpl);
        return "Personnage ajouté à votre compte";
    }
}