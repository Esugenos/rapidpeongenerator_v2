package com.Bulbisoft.RapidPeonGenerator_Back.Controller;

import com.Bulbisoft.RapidPeonGenerator_Back.Controller.Representation.PlayerRepresentation;
import com.Bulbisoft.RapidPeonGenerator_Back.Controller.Representation.PlayerRepresentationFullInfo;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Peon;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Player;
import com.Bulbisoft.RapidPeonGenerator_Back.Service.PlayerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/player")
public class PlayerController {
    PlayerService service;

    public PlayerController(PlayerService service) {
        this.service = service;
    }

    @PostMapping("/create")
    public PlayerRepresentationFullInfo createNewPlayer(@RequestBody PlayerRepresentation body) {
        Player newPlayer = service.addNewPlayer(body);
PlayerRepresentationFullInfo response = new PlayerRepresentationFullInfo(newPlayer.getId(),newPlayer.getPseudo(),
        newPlayer.getPassword(),newPlayer.getTag(), newPlayer.getPeons());
        return response;
    }

    @DeleteMapping("/delete/{id}")
    public String deleteAccount(@PathVariable(value = "id") String id) {
        Optional<PlayerRepresentationFullInfo> player = this.service.getOnePlayerRepresentationFullInfo(id);
        service.deletePlayer(id);
        return player.map(p -> "Votre compte a été supprimé, nous espérons vous revoir bientôt, " + p.getPseudo()).
                orElse("Compte introuvable");

    }

    @GetMapping
    public List<PlayerRepresentation> getAllPlayer() {
        List<PlayerRepresentation> playersR = new ArrayList<>();
        for (Player p : service.getAllPlayers()) {
            PlayerRepresentation pr = new PlayerRepresentation(p.getPseudo(), p.getPassword());
            playersR.add(pr);
        }
        return playersR;
    }

    @GetMapping("/{id}")
    public PlayerRepresentationFullInfo getOnePlayer(@PathVariable(value = "id") String id) {
        Optional<PlayerRepresentationFullInfo> player = this.service.getOnePlayerRepresentationFullInfo(id);
        return player.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Compte introuvable"));
    }

    @GetMapping("/your_peons/{id}")
    public List<Peon> getPeonsOfOnePlayer(@PathVariable(value = "id") String idPlayer) {
        return service.getPeonsforOnePlayer(idPlayer);
    }
}
