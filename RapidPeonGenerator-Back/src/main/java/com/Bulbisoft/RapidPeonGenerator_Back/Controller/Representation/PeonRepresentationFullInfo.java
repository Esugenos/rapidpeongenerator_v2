package com.Bulbisoft.RapidPeonGenerator_Back.Controller.Representation;

import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.ClassPeon;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.Race;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.Sex;

public class PeonRepresentationFullInfo {
    public PeonRepresentationFullInfo() {
    }

    private String id;
    private String name;
    private Sex sex;
    private int age;
    private Race race;
    private ClassPeon classPeon;
    private int strength;
    private int endurance;
    private int dexterity;
    private int intelligence;
    private int luck;

    public PeonRepresentationFullInfo(String id, String name, Sex sex, int age, Race race, ClassPeon classPeon,int strength, int endurance, int dexterity, int intelligence, int luck) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.race = race;
        this.classPeon = classPeon;
        this.strength = strength;
        this.dexterity = dexterity;
        this.endurance = endurance;
        this.intelligence = intelligence;
        this.luck = luck;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public ClassPeon getClassPeon() {
        return classPeon;
    }

    public void setClassPeon(ClassPeon classPeon) {
        this.classPeon = classPeon;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getEndurance() {
        return endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getLuck() {
        return luck;
    }

    public void setLuck(int luck) {
        this.luck = luck;
    }
}
