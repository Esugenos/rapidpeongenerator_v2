package com.Bulbisoft.RapidPeonGenerator_Back.Controller.Representation;

import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.ClassPeon;

public class PeonRepresentationNameClass {


    private String name;
    private ClassPeon classPeon;

    public PeonRepresentationNameClass() {
    }

    public PeonRepresentationNameClass(String name, ClassPeon classPeon) {

        this.name = name;
        this.classPeon = classPeon;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ClassPeon getClassPeon() {
        return classPeon;
    }

    public void setClassPeon(ClassPeon classPeon) {
        this.classPeon = classPeon;
    }
}