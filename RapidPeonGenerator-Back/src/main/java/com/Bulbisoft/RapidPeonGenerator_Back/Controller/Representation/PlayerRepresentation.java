package com.Bulbisoft.RapidPeonGenerator_Back.Controller.Representation;

public class PlayerRepresentation {
    public PlayerRepresentation() {
    }

    String pseudo;
    String password;

    public PlayerRepresentation(String pseudo, String password){
        this.password=password;
        this.pseudo=pseudo;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
