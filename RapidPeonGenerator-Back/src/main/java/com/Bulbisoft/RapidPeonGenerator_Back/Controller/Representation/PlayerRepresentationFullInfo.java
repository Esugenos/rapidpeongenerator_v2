package com.Bulbisoft.RapidPeonGenerator_Back.Controller.Representation;

import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Peon;


import java.util.List;

public class PlayerRepresentationFullInfo {
    public PlayerRepresentationFullInfo() {
    }

    String id;
    String pseudo;
    String password;
    String tag;
    List<Peon> peons;

    public PlayerRepresentationFullInfo(String id, String pseudo, String password, String tag, List<Peon> peons) {
        this.id = id;
        this.password = password;
        this.pseudo = pseudo;
        this.tag = tag;
        this.peons = peons;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<Peon> getPeons() {
        return peons;
    }

    public void setPeons(List<Peon> peons) {
        this.peons = peons;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
