package com.Bulbisoft.RapidPeonGenerator_Back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RapidPeonGeneratorBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(RapidPeonGeneratorBackApplication.class, args);
	}

}
