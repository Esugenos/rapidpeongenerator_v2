package com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain;

import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.Armor_category;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.Armor_type;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.Bonus;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.Rarity;

import javax.persistence.*;

@Entity
@Table(name = "armor")
public class Armor {
    @Id
    private String id;
    private String name;
    @Enumerated(value = EnumType.STRING)
    private Armor_category category;
    @Enumerated(value = EnumType.STRING)
    private Armor_type type;
    @Enumerated(value = EnumType.STRING)
    private Rarity rarity;
    private int defense;
    @Enumerated(value = EnumType.STRING)
    private Bonus bonus;
    private int value;

    public Armor() {
    }

    public Armor(String id, String name, Armor_category category, Armor_type type, Rarity rarity,
                 int defense, Bonus bonus, int value) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.type = type;
        this.rarity = rarity;
        this.defense = defense;
        this.bonus = bonus;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Armor_category getCategory() {
        return category;
    }

    public void setCategory(Armor_category category) {
        this.category = category;
    }

    public Armor_type getType() {
        return type;
    }

    public void setType(Armor_type type) {
        this.type = type;
    }

    public Rarity getRarity() {
        return rarity;
    }

    public void setRarity(Rarity rarity) {
        this.rarity = rarity;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public Bonus getBonus() {
        return bonus;
    }

    public void setBonus(Bonus bonus) {
        this.bonus = bonus;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
