package com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum;

public enum ClassPeon {
    PEON,
    WARRIOR,
    HUNTER,
    MAGUS,
    PRIEST,
    ROGUE;
}
