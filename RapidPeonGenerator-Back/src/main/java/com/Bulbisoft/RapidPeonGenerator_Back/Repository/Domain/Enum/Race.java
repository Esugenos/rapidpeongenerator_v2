package com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum;

public enum Race {
    ELF, ORC, HUMAN, DWARF;
}
