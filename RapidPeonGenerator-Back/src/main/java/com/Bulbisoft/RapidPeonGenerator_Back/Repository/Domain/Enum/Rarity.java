package com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum;

public enum Rarity {
    COMMON,
    RARE,
    EPIC,
    LEGENDARY;
}
