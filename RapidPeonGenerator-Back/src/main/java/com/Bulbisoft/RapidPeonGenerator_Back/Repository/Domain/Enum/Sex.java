package com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum;

public enum Sex {
    FEMALE,
    MALE;
}
