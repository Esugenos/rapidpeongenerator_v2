package com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum;

public enum Weapon_category {
    SWORD(1),
    CLAYMORE(2),
    KATANA(2),
    HATCHET(1),
    AXE(2),
    SHIELD(1),
    THROW(2),
    BOW(2),
    CROSSBOW(2),
    DAGGER(1);

    private int hand;

    Weapon_category(int hand){
        this.hand = hand;
    }

    public int getHand() {
        return hand;
    }
}
