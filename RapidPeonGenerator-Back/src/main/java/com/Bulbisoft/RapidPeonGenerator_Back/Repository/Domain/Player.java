package com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Player {
    @Id
    String id;
    String pseudo;
    String tag;
    String password;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_player")
    List<Peon> peons = new ArrayList<>();

    public Player() {
    }

    public Player(String id, String pseudo,  String password, String tag, List<Peon> peons) {
        this.id = id;
        this.pseudo = pseudo;
        this.tag = tag;
        this.password=password;
        this.peons = peons;
    }

    public void addPeonToPlayer(Peon peon) {
        peons.add(peon);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<Peon> getPeons() {
        return peons;
    }

    public void setPeons(List<Peon> peons) {
        this.peons = peons;
    }
}
