package com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain;

import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.Bonus;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "relic")
public class Relic {
    @Id
    private String id;
    private String name;
    private int attack;
    private int defense;
    private Bonus bonus;
    private int value;

    public Relic(String id, String name, int attack, int defense, Bonus bonus, int value) {
        this.id = id;
        this.name = name;
        this.attack = attack;
        this.defense = defense;
        this.bonus = bonus;
        this.value = value;
    }

    public Relic() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public Bonus getBonus() {
        return bonus;
    }

    public void setBonus(Bonus bonus) {
        this.bonus = bonus;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
