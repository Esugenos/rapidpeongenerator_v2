package com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain;

import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.Bonus;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.Rarity;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.Weapon_category;

public class Weapon {

    private String id;
    private String name;
    private Weapon_category category;
    private Rarity rarity;
    private int attack;
    private int range;
    private int speed;
    private int critical;
    private Bonus bonus;
    private int value;

    public Weapon(String id, String name, Weapon_category category, Rarity rarity, int attack, int range,
                  int speed, int critical, Bonus bonus, int value) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.rarity = rarity;
        this.attack = attack;
        this.range = range;
        this.speed = speed;
        this.critical = critical;
        this.bonus = bonus;
        this.value = value;
    }

    public Weapon(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Weapon_category getCategory() {
        return category;
    }

    public void setCategory(Weapon_category category) {
        this.category = category;
    }

    public Rarity getRarity() {
        return rarity;
    }

    public void setRarity(Rarity rarity) {
        this.rarity = rarity;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getCritical() {
        return critical;
    }

    public void setCritical(int critical) {
        this.critical = critical;
    }

    public Bonus getBonus() {
        return bonus;
    }

    public void setBonus(Bonus bonus) {
        this.bonus = bonus;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
