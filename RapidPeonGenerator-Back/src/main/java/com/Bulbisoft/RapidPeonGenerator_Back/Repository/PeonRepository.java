package com.Bulbisoft.RapidPeonGenerator_Back.Repository;

import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Peon;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PeonRepository extends CrudRepository<Peon, String> {
    Optional<Peon> findByName(String name);
}
