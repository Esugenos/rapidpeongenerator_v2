package com.Bulbisoft.RapidPeonGenerator_Back.Repository;

import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Player;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends CrudRepository<Player, String> {

}
