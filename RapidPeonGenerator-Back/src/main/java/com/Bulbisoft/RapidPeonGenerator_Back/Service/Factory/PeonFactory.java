package com.Bulbisoft.RapidPeonGenerator_Back.Service.Factory;

import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.ClassPeon;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Peon;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.Race;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.Sex;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.*;

import static java.util.Collections.shuffle;

@Component
public class PeonFactory {
    HashMap<Integer, Integer> attributs = new HashMap();
    int points;
    int strength;
    int endurance;
    int dexterity;
    int intelligence;
    int luck;

    public PeonFactory() {

    }

    public Peon generateManualPeon(String name, Sex sex, int age, Race race, ClassPeon classPeon, int strength, int endurance, int dexterity, int intelligence, int luck) {
        String id = UUID.randomUUID().toString();

        return new Peon(id, name, sex, age, race, classPeon, strength, endurance, dexterity, intelligence, luck);
    }

    public Peon generateAutoPeon(String name, ClassPeon classPeon) throws IOException {
        if(name == null){
            BufferedReader fileName = new BufferedReader(new FileReader ("C:\\Users\\NEDELEC\\Desktop\\RapidPeonGenerator_V2\\rapidpeongenerator-back\\src\\main\\resources\\11000_general_names.txt"));
            int line = Math.abs(new Random().nextInt(11000));
            for (int i=0 ; i<line ; i++)
                fileName.readLine();
            name = fileName.readLine();
        }
        String id = UUID.randomUUID().toString();
        Sex sex = chooseAleaSex();
        int age = Math.abs(new Random().nextInt(99));
        Race race = chooseAleaRace();
        for (int i = 0; i < 5; i++) {
            attributs.put(i, 0);
        }
        distributeAttributs(classPeon);

        return new Peon(id, name, sex, age, race, classPeon, strength, endurance, dexterity, intelligence, luck);
    }

    private Sex chooseAleaSex() {
        List<Sex> collectionSex = new ArrayList<Sex>(List.of(Sex.MALE, Sex.FEMALE));
        shuffle(collectionSex);
        int r = Math.abs(new Random().nextInt(2));
        return collectionSex.get(r);
    }

    private Race chooseAleaRace() {
        List<Race> collectionRace = new ArrayList<Race>(List.of(Race.DWARF, Race.ELF, Race.HUMAN, Race.ORC));
        shuffle(collectionRace);
        int r = Math.abs(new Random().nextInt(4));
        return collectionRace.get(r);
    }

    private void distributeAttributs(ClassPeon classPeon) {
        switch (classPeon) {
            case PEON:
                distributeAttributsIfItsPeon();
                break;
            case WARRIOR:
                distributeAttributsIfItsWarrior();
                break;
            case HUNTER:
                distributeAttributsIfItsHunter();
                break;
            case MAGUS:
                distributeAttributsIfItsMagus();
                break;
            case ROGUE:
                distributeAttributsIfItsRogue();
                break;
            case PRIEST:
                distributeAttributsIfItsPriest();
                break;
        }
    }

    private void distributeAttributsIfItsPeon() {
        points = 300;
        while (points > 0) {
            int key = Math.abs(new Random().nextInt(5));
            if (attributs.get(key) < 100) {
                if (attributs.get(key) < 10) {
                    int value = Math.abs(new Random().nextInt(90));
                    attributs.put(key, attributs.get(key) + value);
                    points -= value;
                } else {
                    attributs.put(key, attributs.get(key) + 1);
                    points--;
                }
            }
        }
        strength = attributs.get(0);
        endurance = attributs.get(1);
        dexterity = attributs.get(2);
        intelligence = attributs.get(3);
        luck = attributs.get(4);
    }

    private void distributeAttributsIfItsWarrior() {
        distributePointsForSpecificsClass();
        strength = attributs.get(0);
        endurance = attributs.get(1);
        dexterity = attributs.get(2);
        intelligence = attributs.get(3);
        luck = attributs.get(4);
    }

    private void distributeAttributsIfItsHunter() {
        distributePointsForSpecificsClass();
        dexterity = attributs.get(0);
        luck = attributs.get(1);
        strength = attributs.get(2);
        intelligence = attributs.get(3);
        endurance = attributs.get(4);
    }

    private void distributeAttributsIfItsRogue() {
        distributePointsForSpecificsClass();
        dexterity = attributs.get(0);
        luck = attributs.get(1);
        strength = attributs.get(2);
        intelligence = attributs.get(3);
        endurance = attributs.get(4);
    }

    private void distributeAttributsIfItsMagus() {
        distributePointsForSpecificsClass();
        intelligence = attributs.get(0);
        luck = attributs.get(1);
        dexterity = attributs.get(2);
        strength = attributs.get(3);
        endurance = attributs.get(4);
    }

    private void distributeAttributsIfItsPriest() {
        distributePointsForSpecificsClass();
        intelligence = attributs.get(0);
        luck = attributs.get(1);
        dexterity = attributs.get(2);
        strength = attributs.get(3);
        endurance = attributs.get(4);
    }
    private void distributePointsForSpecificsClass(){
        points = 300;
        for (int i = 0; i < 2; i++) {
            int value = 90 + Math.abs(new Random().nextInt(36));
            attributs.put(i, value);
            points -= value;
        }
        while (points > 0) {
            int key = 2 + Math.abs(new Random().nextInt(3));
            if (attributs.get(key) < 100) {
                if (attributs.get(key) < 10) {
                    int value = Math.abs(new Random().nextInt(90));
                    attributs.put(key, attributs.get(key) + value);
                    points -= value;
                } else {
                    attributs.put(key, attributs.get(key) + 1);
                    points--;
                }
            }
        }
    }
}
