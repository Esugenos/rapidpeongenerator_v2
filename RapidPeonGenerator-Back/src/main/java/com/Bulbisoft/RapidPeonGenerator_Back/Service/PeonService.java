package com.Bulbisoft.RapidPeonGenerator_Back.Service;

import com.Bulbisoft.RapidPeonGenerator_Back.Controller.Representation.PeonRepresentation;
import com.Bulbisoft.RapidPeonGenerator_Back.Controller.Representation.PeonRepresentationFullInfo;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Enum.ClassPeon;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Peon;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.PeonRepository;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.PlayerRepository;
import com.Bulbisoft.RapidPeonGenerator_Back.Service.Factory.PeonFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class PeonService {
    PeonRepository peonRepository;
    PeonFactory peonFactory;
    PlayerRepository playerRepository;

    public PeonService(PeonRepository peonRepository, PeonFactory peonFactory, PlayerRepository playerRepository) {
        this.peonRepository = peonRepository;
        this.peonFactory = peonFactory;
        this.playerRepository = playerRepository;
    }

    public PeonRepresentationFullInfo createNewAutoPeon(String name, ClassPeon classPeon) throws IOException {
        Peon peon = peonFactory.generateAutoPeon(name,classPeon);
        PeonRepresentationFullInfo peonFull = new PeonRepresentationFullInfo(peon.getId(), peon.getName(), peon.getSex(),
                peon.getAge(), peon.getRace(), peon.getClassPeon(),peon.getStrength(), peon.getEndurance(), peon.getDexterity(),
                peon.getIntelligence(), peon.getLuck());
        peonRepository.save(peon);
        return peonFull;
    }

    public PeonRepresentationFullInfo createManualPeon(PeonRepresentation peon) {
        Peon newPeon = peonFactory.generateManualPeon(peon.getName(), peon.getSex(), peon.getAge(), peon.getRace(),
                peon.getClassPeon(),peon.getStrength(), peon.getEndurance(), peon.getDexterity(), peon.getIntelligence(), peon.getLuck());
        PeonRepresentationFullInfo peonFull = new PeonRepresentationFullInfo(newPeon.getId(), newPeon.getName(), newPeon.getSex(),
                newPeon.getAge(), newPeon.getRace(), newPeon.getClassPeon(),newPeon.getStrength(), newPeon.getEndurance(), newPeon.getDexterity(),
                newPeon.getIntelligence(), newPeon.getLuck());
        peonRepository.save(newPeon);
        return peonFull;
    }

    public void deletePeonById(String id) {
        peonRepository.deleteById(id);
    }

    public List<Peon> getAllPeons() {
        List<Peon> peons = new ArrayList<>();
        for (Peon p : peonRepository.findAll()) {
            peons.add(p);
        }
        return peons;
    }

    public Optional<Peon> getOnePeonById(String id) {
        Optional<Peon> peon = peonRepository.findById(id);
        return peon;
    }

    public void AddPeonToPlayer(String idPeon, String idPlayer) {

        playerRepository.findById(idPlayer).ifPresent(p -> {
            peonRepository.findById(idPeon).ifPresent(pe -> p.addPeonToPlayer(pe));
            playerRepository.save(p);
        });
    }
}
