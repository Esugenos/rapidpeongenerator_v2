package com.Bulbisoft.RapidPeonGenerator_Back.Service;

import com.Bulbisoft.RapidPeonGenerator_Back.Controller.Representation.PlayerRepresentation;
import com.Bulbisoft.RapidPeonGenerator_Back.Controller.Representation.PlayerRepresentationFullInfo;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Peon;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.Domain.Player;
import com.Bulbisoft.RapidPeonGenerator_Back.Repository.PlayerRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import java.util.*;

@Component
public class PlayerService {
    PlayerRepository playerRepository;

    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;

    }

    public Player addNewPlayer(PlayerRepresentation playerRepresentation) {
        String id = UUID.randomUUID().toString();
        String tag = "#" + id.substring(0, 5);
        Player player = new Player(id, playerRepresentation.getPseudo(), playerRepresentation.getPassword(), tag, null);
        playerRepository.save(player);
        return player;
    }

    public void deletePlayer(String id) {
        playerRepository.deleteById(id);
    }

    public Optional<PlayerRepresentationFullInfo> getOnePlayerRepresentationFullInfo(String idPlayerR) {
        Optional<Player> player = getOnePlayer(idPlayerR);
        return player.map(p -> new PlayerRepresentationFullInfo(p.getId(),p.getPseudo(), p.getPassword(),p.getTag(),p.getPeons()));
    }

    private Optional<Player> getOnePlayer(String idPlayer) {
        Optional<Player> player = playerRepository.findById(idPlayer);
        return player;
    }
    public List<Peon> getPeonsforOnePlayer(String idPlayer){
        List<Peon> peonsOfPlayer = new ArrayList<>();
        Optional<Player> player = playerRepository.findById(idPlayer);
        player.ifPresent(player1 -> {for(Peon peon: player1.getPeons()){
        peonsOfPlayer.add(peon);}
        });
        return peonsOfPlayer;
    }

    public List<Player> getAllPlayers() {
        List<Player> players = new ArrayList<>();
        for (Player p : playerRepository.findAll()) {
            players.add(p);
        }
        return players;
    }
}


