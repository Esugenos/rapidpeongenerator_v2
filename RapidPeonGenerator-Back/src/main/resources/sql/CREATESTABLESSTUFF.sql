CREATE TABLE armor (
id text not null PRIMARY KEY,
name text not null,
category text not null,
type text not null,
rarity text not null,
defense integer not null,
bonus text,
value integer not null
);

CREATE TABLE weapon (
id text not null PRIMARY KEY,
name text not null,
category text not null,
rarity text not null,
attack integer not null,
range integer not null,
speed integer not null,
critical integer not null,
bonus text,
value integer not null
)