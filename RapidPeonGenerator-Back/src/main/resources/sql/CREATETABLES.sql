CREATE TABLE player(
id text not null PRIMARY KEY,
pseudo text not null,
password text not null,
tag text not null
);

CREATE TABLE peon(
id text not null PRIMARY KEY,
name text not null,
sex text not null,
age integer not null,
race text not null,
classPeon text not null,
strength integer not null,
dexterity integer not null,
endurance integer not null,
intelligence integer not null,
luck integer not null,
id_player text REFERENCES player(id)
)