import {Component, Input} from '@angular/core';
import {BodyPlayerFullInfoModel} from './model/bodyPlayerFullInfo.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'RapidPeonGenerator-Front';
  @Input()
  name: string;
  playerInfo: BodyPlayerFullInfoModel;

  constructor() {

  }

  loginCreate(event: BodyPlayerFullInfoModel) {
    console.log(event);
    this.playerInfo = event;
  }
}
