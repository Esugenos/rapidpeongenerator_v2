import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AccueilComponent } from './component/accueil/accueil.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import { PeonCreationComponent } from './component/peon-creation/peon-creation.component';
import { CharacterSheetComponent } from './component/character-sheet/character-sheet.component';
import {PeonService} from './service/peon.service';
import { CreateManualPeonComponent } from './component/create-manual-peon/create-manual-peon.component';
import { NewPlayerRegisterComponent } from './component/new-player-register/new-player-register.component';
import {PlayerService} from './service/playerService';
import { HeaderComponent } from './component/header/header.component';



const appRoutes: Routes = [
  { path: '', component: AccueilComponent },
  { path: 'peons/create', component:  PeonCreationComponent},
  { path: 'peons/createmanual', component:  CreateManualPeonComponent},
  { path: 'player/newregister', component:  NewPlayerRegisterComponent},
  ];

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    PeonCreationComponent,
    CharacterSheetComponent,
    CreateManualPeonComponent,
    NewPlayerRegisterComponent,
    HeaderComponent,


  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes),
    FormsModule
  ],
  providers: [PeonService, PlayerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
