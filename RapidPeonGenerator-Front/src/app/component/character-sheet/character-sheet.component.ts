import {Component, Input, OnInit} from '@angular/core';
import {PeonModel} from '../../model/peon.model';

@Component({
  selector: 'app-character-sheet',
  templateUrl: './character-sheet.component.html',
  styleUrls: ['./character-sheet.component.css']
})
export class CharacterSheetComponent implements OnInit {
  @Input() newPeon: PeonModel;

  constructor() { }

  ngOnInit() {
  }

}
