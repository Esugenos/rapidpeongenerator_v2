import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateManualPeonComponent } from './create-manual-peon.component';

describe('CreateManualPeonComponent', () => {
  let component: CreateManualPeonComponent;
  let fixture: ComponentFixture<CreateManualPeonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateManualPeonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateManualPeonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
