import {Component, OnInit} from '@angular/core';
import {PeonService} from '../../service/peon.service';
import {BodyNewManualPeonModel} from '../../model/bodyNewManualPeon.model';

@Component({
  selector: 'app-create-manual-peon',
  templateUrl: './create-manual-peon.component.html',
  styleUrls: ['./create-manual-peon.component.css']
})
export class CreateManualPeonComponent implements OnInit {
  strength = 0;
  dexterity = 0;
  endurance = 0;
  intelligence = 0;
  luck = 0;
  counter = 300;
  max = 300;
  name: string;
  age: number;
  race: string;
  sex: string;
  classPeon: string;
  warningAttr = 'Vous avez dépensé tout vos points!';
  disabled = true;
  showWarning = false;
  showManualPage = true;
  showSheet = false;
  returnNewPeon;

  constructor(private service: PeonService) {
  }

  ngOnInit() {
  }

  change() {
    if (this.counter > 1) {
      this.counter = 300 - this.strength - this.dexterity - this.endurance - this.intelligence - this.luck;
      this.max = 300;
      this.showWarning = false;
    } else {
      this.counter = 300 - this.strength - this.dexterity - this.endurance - this.intelligence - this.luck;
      this.max = 0;
      this.showWarning = true;
    }


    if (this.counter === 0 && this.name != null && this.age !== null) {
      this.disabled = false;
    } else {
      this.disabled = true;
    }
  }

  chooseRace(race: string) {
    this.race = race;
  }

  chooseSex(sex: string) {
    this.sex = sex;
  }
  chooseClass(classPeon: string) {
    this.classPeon = classPeon;
  }

  createManualPeon() {
    const newPeon = new BodyNewManualPeonModel(this.name, this.sex, this.age, this.race,
      this.classPeon, this.strength, this.dexterity, this.endurance, this.intelligence, this.luck);
    this.service.createManualPeon(newPeon).subscribe(reponse => this.returnNewPeon = reponse);
    this.showManualPage = false;
    this.showSheet = true;
  }

  /* if (this.counter > 0) {
     this.counter = 300 - this.strength - this.dexterity - this.endurance - this.intelligence - this.luck;
     this.showWarning = false;
     this.disabled = true;
   } else if (this.counter < 0) {
     this.counter = 300 - this.strength - this.dexterity - this.endurance - this.intelligence - this.luck;
     this.showWarning = true;
     this.disabled = true;
   }
   if ( this.counter === 0 && this.name != null && this.age !== null) {
     this.counter = 300 - this.strength - this.dexterity - this.endurance - this.intelligence - this.luck;
     this.showWarning = false;
     this.disabled = false;
   }*/

}
