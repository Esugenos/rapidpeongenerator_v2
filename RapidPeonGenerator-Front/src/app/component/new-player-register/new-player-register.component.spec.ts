import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPlayerRegisterComponent } from './new-player-register.component';

describe('NewPlayerRegisterComponent', () => {
  let component: NewPlayerRegisterComponent;
  let fixture: ComponentFixture<NewPlayerRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPlayerRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPlayerRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
