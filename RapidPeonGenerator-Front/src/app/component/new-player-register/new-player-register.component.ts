import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PlayerService} from '../../service/playerService';
import {BodyNewPlayerModel} from '../../model/bodyNewPlayer.model';
import {BodyPlayerFullInfoModel} from '../../model/bodyPlayerFullInfo.model';

@Component({
  selector: 'app-new-player-register',
  templateUrl: './new-player-register.component.html',
  styleUrls: ['./new-player-register.component.css']
})
export class NewPlayerRegisterComponent implements OnInit {
  pseudo: string;
  name: string;
  password: string;
  password2: string;
  disabled = true;
  showWarnPassword = false;
  showMessage = false;
  showInput = true;
  ResponseServer: BodyPlayerFullInfoModel;
  @Output() Emitter = new EventEmitter<BodyPlayerFullInfoModel>();


  constructor(private service: PlayerService) {
  }

  ngOnInit() {
  }

  change() {
    if (this.password2 == null || this.password2 === '' || this.password == null || this.password === '') {
      this.showWarnPassword = false;
      this.disabled = true;
    } else if (this.password2 !== this.password) {
      this.showWarnPassword = true;
      this.disabled = true;
    } else if (this.password2 === this.password) {
      this.showWarnPassword = false;
      if (this.pseudo != null && this.pseudo !== '') {
        this.disabled = false;
      }
    }
  }

  createPlayer() {
    const newPlayer = new BodyNewPlayerModel(this.pseudo, this.password);
    this.service.createNewPlayer(newPlayer).subscribe(reponse => {
      this.ResponseServer = reponse;
      this.Emitter.emit(this.ResponseServer);
    });
    this.showInput = false;
    this.showMessage = true;
    this.name = this.pseudo;

  }
}
