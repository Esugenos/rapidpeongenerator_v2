import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeonCreationComponent } from './peon-creation.component';

describe('PeonCreationComponent', () => {
  let component: PeonCreationComponent;
  let fixture: ComponentFixture<PeonCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeonCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeonCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
