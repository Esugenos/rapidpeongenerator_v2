import {Component, Input, OnInit} from '@angular/core';
import {PeonModel} from '../../model/peon.model';
import {BodyNameClassPeonModel} from '../../model/bodyNameClassPeon.model';
import {PeonService} from '../../service/peon.service';

@Component({
  selector: 'app-peon-creation',
  templateUrl: './peon-creation.component.html',
  styleUrls: ['./peon-creation.component.css']
})
export class PeonCreationComponent implements OnInit {
show = false;
showButtons1 = true;
showSheet = false;
name = null;
classPeon = 'PEON';
returnNewPeon: PeonModel;
disabled = 'disabled';

  constructor(private service: PeonService) { }

  ngOnInit() {
  }
  enterName() {
    this.show = true;
    this.showButtons1 = false;
  }

  change() {
    this.disabled = '';
  }
  chooseClass(classPeon: string) {
    this.classPeon = classPeon;
}
  clicAuto() {
    const newPeon = new BodyNameClassPeonModel(this.name, this.classPeon);
    this.service.createAutoPeon(newPeon).subscribe(reponse => {
     this.returnNewPeon = reponse;
    });
    this.showSheet = true;
    this.show = false;
    console.log(this.classPeon);


  }

}
