import {PeonModel} from './peon.model';

export class BodyPlayerFullInfoModel {
  peons: PeonModel[];
  pseudo: string;
  password: string;
  tag: string;
  id: string;

  constructor(id: string, pseudo: string, password: string, tag: string, peons: PeonModel[]) {
    this.id = id;
    this.pseudo = pseudo;
    this.password = password;
    this.tag = tag;
    this.peons = peons;
  }
}
