export class PeonModel {
  id: string;
  name: string;
  sex: string;
  age: number;
  race: string;
  classPeon: string;
  strength: number;
  endurance: number;
  dexterity: number;
  intelligence: number;
  luck: number;

  constructor(id: string, name: string, sex: string, age: number, race: string, classPeon: string, strength: number,
              endurance: number, dexterity: number, intelligence: number, luck: number) {
    this.id = id;
    this.name = name;
    this.sex = sex;
    this.age = age;
    this.race = race;
    this.classPeon = classPeon;
    this.strength = strength;
    this.endurance = endurance;
    this.dexterity = dexterity;
    this.intelligence = intelligence;
    this.luck = luck;
  }
}
