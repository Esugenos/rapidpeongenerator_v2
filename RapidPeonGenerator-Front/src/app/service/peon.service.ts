import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BodyNameClassPeonModel} from '../model/bodyNameClassPeon.model';
import {PeonModel} from '../model/peon.model';
import {Observable} from 'rxjs';
import {BodyNewManualPeonModel} from '../model/bodyNewManualPeon.model';

@Injectable()
export class PeonService {
  constructor(private http: HttpClient) {
  }


  createAutoPeon(body: BodyNameClassPeonModel): Observable<PeonModel> {
    return this.http.post<PeonModel>('http://localhost:8880/peons/create/auto', body);
  }

  createManualPeon(body: BodyNewManualPeonModel): Observable<PeonModel> {
    return this.http.post<PeonModel>('http://localhost:8880/peons/create/manual', body);
  }
}
