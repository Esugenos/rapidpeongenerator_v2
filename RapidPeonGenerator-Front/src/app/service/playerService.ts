import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BodyNameClassPeonModel} from '../model/bodyNameClassPeon.model';
import {Observable} from 'rxjs';
import {PeonModel} from '../model/peon.model';
import {BodyNewPlayerModel} from '../model/bodyNewPlayer.model';
import {BodyPlayerFullInfoModel} from '../model/bodyPlayerFullInfo.model';



@Injectable()
export class PlayerService {
  constructor(private http: HttpClient) {
  }

  createNewPlayer(body: BodyNewPlayerModel): Observable<BodyPlayerFullInfoModel> {
    return this.http.post<BodyPlayerFullInfoModel>('http://localhost:8880/player/create', body);
  }
}
